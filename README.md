Video testing tool for use with a raspberry pi or pi zero.

Designed and tested with pizero and Adafruit OLED bonnet (https://www.adafruit.com/product/3531)

See releases for device image. 

On boot, device will start playing all videos in the "/home/pi/videos" directory and loop indefinitely. Using the OLED navigation on the device, the user can choose to loop a specific file if they desire as well. There are also options to get HDMI sync information for help in debugging issues. 

Code is not pristine, but it does work.


To connect to a wifi network and load more videos:
- Mount SD Card in secondary computer
- Add a file called "wpa_supplicant.conf"
- Edit file with your SSID and password (see below for format)
- Unmount, put back in the RPi and it should connect on boot. (The Device Info menu item will give you the IP details)
- SSH is enabled by default, so you can connect via SFTP on port 22 via any FTP tool (default username: pi - default pass: raspberry). Videos should be loaded to the /home/pi/videos directory.

### WPA Supplicant sample:
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
     ssid="Your network name/SSID"
     psk="Your WPA/WPA2 security key"
     key_mgmt=WPA-PSK
}
```